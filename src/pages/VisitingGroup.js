import React from "react";
import { Link } from "react-router-dom";

function VisitingGroup() {
    return (
        <body className="bg-black">
            <section className="bg-black">
                <div class="items-center justify-center py-8 mx-auto h-screen md:h-screen lg:py-0 bg-hero bg-no-repeat bg-cover bg-center">
                    <div className="h-screen bg-black px-6 md:px-36 lg:px-56 bg-opacity-75 py-12 md:py-14 xl:py-24">
                        <h1 class="mb-5 text-xl font-extrabold tracking-tight leading-none text-white md:text-2xl xl:text-2xl lg:text-2xl dark:text-white">Pilih Tanggal Kunjungan</h1>
                        <input type="datetime-local" className="w-full mb-6 radio rounded-lg  caret-white"/>
                        <h1 class="mb-5 text-xl font-extrabold tracking-tight leading-none text-white md:text-2xl xl:text-2xl lg:text-2xl dark:text-white">Data Kelompok</h1>
                        <input type="text" className="w-full mb-5 radio rounded-lg bg-transparent text-white placeholder-white  caret-white" placeholder="Lembaga"/>
                        <input type="text" className="w-full mb-5 radio rounded-lg bg-transparent text-white placeholder-white  caret-white" placeholder="Penanggung Jawab"/>
                        <input type="text" className="w-full mb-5 radio rounded-lg bg-transparent text-white placeholder-white  caret-white" placeholder="Email"/>
                        <input type="text" className="w-full mb-5 radio rounded-lg bg-transparent text-white placeholder-white  caret-white" placeholder="Nomor Telepon"/>
                        <input type="text" className="w-full mb-5 radio rounded-lg bg-transparent text-white placeholder-white  caret-white" placeholder="Kota"/>
                        <input type="text" className="w-full mb-5 radio rounded-lg bg-transparent text-white placeholder-white  caret-white" placeholder="Jumlah Peseta > 5"/>
                        <button type="button" class="w-full  text-black mt-5 md:mt-9 bg-white hover:bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-primary-600 dark:hover:bg-primary-700 dark:focus:ring-primary-800">
                            <Link to="/ListVisiting">Kirim</Link>
                        </button>
                        <button type="button" class="w-full text-white mt-5 md:mt-9 border-white border-2 hover:bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-primary-600 dark:hover:bg-primary-700 dark:focus:ring-primary-800">
                            <Link to="/SelectTypeVisiting">Kembali</Link>
                        </button>
                    </div>
                </div>
            </section>
            <section className="bg-black h-96"/>
        </body>
    )
}

export default VisitingGroup