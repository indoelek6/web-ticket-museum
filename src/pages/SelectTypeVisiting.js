import React, { useState } from "react";
import { Link } from "react-router-dom";

const message = [
    {
        text: 'Wajib registrasi online sebelum masuk ke ruang pamer.'
    },
    {
        text: 'Pastikan tiba di Galeri Nasional Indonesia paling lambat 15 menit sebelum sesi kunjungan yang dipilih.'
    },
    {
        text: 'Kode booking hangus apabila sudah melebihi 30 menit setelah sesi dimulai dan pengunjung yang terlambat dapat melakukan registrasionline ulang dengan memilih sesi yang masih tersedia.'
    },
    {
        text: 'Pindai QR code registrasi pameran atau tunjukkan kode booking registrasi pameran kepada petugas registrasi pameran.'
    },
]

function SelectTypeVisiting() {

    const [click, setClick] = useState(false)

    return (
        <>
        <nav class="bg-black px-6 sm:px-4 py-2.5 dark:bg-gray-900 fixed w-full z-20 top-0 left-0  dark:border-gray-600">
            <div class="container flex flex-wrap items-center justify-between mx-auto">
                <span className="self-center text-xl font-semibold whitespace-nowrap text-white">Museum</span>
            </div>
        </nav>
        <body className="bg-black">
            <section className="bg-black">
                <div class="items-center justify-center py-8 mx-auto h-screen md:h-screen lg:py-0 bg-hero bg-no-repeat bg-cover bg-center">
                    <div className="h-screen bg-black px-6 md:px-36 lg:px-56 bg-opacity-75 py-12 md:py-14 xl:py-24">
                        <h1 class="mb-10 text-4xl font-extrabold tracking-tight leading-none text-white md:text-5xl xl:text-7xl lg:text-6xl dark:text-white">Persyaratan Kunjungan<br/>untuk Pengunjung</h1>
                        {message.map((e, index) => (
                            <div className="flex flex-row">
                                <h1 className="text-sm mb-3 tracking-tight leading-none text-white md:text-lg dark:text-white xl:text-2xl">{index + 1}.</h1>
                                <p className="text-sm mb-3 ml-3 tracking-tight leading-none text-white md:text-lg dark:text-white xl:text-2xl">
                                    {e.text}
                                </p>
                            </div>
                        ))}
                         <div class="flex items-center justify-between mt-3">
                            <div class="flex items-start">
                                <div class="flex items-center h-5">
                                    <input id="remember" aria-describedby="remember" type="checkbox" class="w-4 h-4 border border-gray-300 rounded bg-gray-50 focus:ring-3 focus:ring-primary-300 dark:bg-gray-700 dark:border-gray-600 dark:focus:ring-primary-600 dark:ring-offset-gray-800" required="" onClick={() => setClick(!click)}/>
                                </div>
                                <div class="ml-3 text-sm">
                                    <label for="remember" class="text-white dark:text-gray-300 lg:text-xl">Menyetujui</label>
                                </div>
                            </div>
                        </div>
                        {click == true &&
                        <>
                            <button type="button" class="w-full  text-white mt-5 md:mt-9 border-white border-2 hover:bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-primary-600 dark:hover:bg-primary-700 dark:focus:ring-primary-800">
                                <Link to="/VisitingPersonal">Kunjungan Perorangan</Link>
                            </button>
                            <button type="button" class="w-full text-white mt-5 md:mt-9 border-white border-2 hover:bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-primary-600 dark:hover:bg-primary-700 dark:focus:ring-primary-800">
                                <Link to="/VisitingGroup">Kunjungan Kelompok > 5</Link>
                            </button>
                        </>
                        }   
                    </div>
                </div>
            </section>
            <section className="bg-black h-96"/>
        </body>
        </>
    )
}

export default SelectTypeVisiting