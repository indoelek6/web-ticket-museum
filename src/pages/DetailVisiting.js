import React from "react";
import { Link, useLocation } from "react-router-dom";


function DetailVisiting(){

    const location = useLocation()

    const data = location.state

    console.log(data);

    return (
        <body className="bg-black">
            <section className="bg-black">
                <div class="items-center justify-center py-8 mx-auto h-screen md:h-screen lg:py-0 bg-hero bg-no-repeat bg-cover bg-center">
                    <div className="h-screen bg-black px-6 md:px-36 lg:px-56 bg-opacity-75 py-12 md:py-14 xl:py-24">
                        <h1 class="mb-10 text-4xl font-extrabold tracking-tight leading-none text-white md:text-5xl xl:text-7xl lg:text-6xl dark:text-white">Detail Kujungan<br/>Perorang</h1>
                        <div className="w-full h-auto bg-white rounded-md p-5">
                            <div className="flex flex-row justify-between">
                                <h1 class="mb-5 text-md font-normal tracking-tight leading-none text-black md:text-lg dark:text-white">Code {data == 0 ? '32451' : '5571' }</h1>
                                <h1 class="mb-5 text-md font-normal tracking-tight leading-none text-black md:text-lg dark:text-white">{data == 0 ? '12 Desember 2022 15:30' : '1 Januari 2022 16:00'}</h1>
                            </div>
                            <h1 class=" text-lg font-semibold tracking-tight leading-none text-black md:text-xl dark:text-white">{data == 0 ? 'Ihsani Hamidah' : "Pengajian Annurahman"}</h1>
                            <h1 class="text-md font-normal tracking-tight leading-none text-black md:text-lg dark:text-white">{data == 0 ? '21 Tahun' : 'Elsa Syarif'}</h1>
                            <h1 class=" text-md font-normal tracking-tight leading-none text-black md:text-lg dark:text-white">{data == 0 ? 'Wanita' : 'pengajianannurahman@gmail.com'}</h1>
                            <br/>
                            <h1 class=" mb-2 text-lg font-semibold tracking-tight leading-none text-black md:text-xl dark:text-white">{data == 0 ?'Warga Negara Indonesia (WNI)' : '0857832129'}</h1>
                            <h1 class="text-md font-normal tracking-tight leading-none text-black md:text-lg dark:text-white">{data == 0 ? 'ihsanihamidah@gmail.com' : 'Yogyakarta'}</h1>
                            <h1 class="text-md font-normal tracking-tight leading-none text-black md:text-lg dark:text-white">{data == 0 ? '087886831322' : '8 Orang'}</h1>
                            <Link type="button" class="mt-10 mb-3 w-full text-white bg-black hover:bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-primary-600 dark:hover:bg-primary-700 dark:focus:ring-primary-800" onClick={() => alert('Berhasil Di Cetak')}>
                                Cetak
                            </Link>
                            <Link type="button" to="/ListVisiting" class="w-full text-black border-2 border-black hover:bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-primary-600 dark:hover:bg-primary-700 dark:focus:ring-primary-800">
                                Kembali
                            </Link>
                        </div>
                    </div>
                </div>
            </section>
            <section className="bg-black h-96"/>
        </body>
    )
}

export default DetailVisiting;