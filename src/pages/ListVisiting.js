import React from "react";
import { Link } from "react-router-dom";

function ListVisiting() {
    return (
        <>
        <nav class="bg-black px-2 sm:px-4 py-2.5 dark:bg-gray-900 fixed w-full z-20 top-0 left-0  dark:border-gray-600">
            <div class="container flex flex-wrap items-center justify-between mx-auto">
                <Link to='/' class="flex items-center">
                    <span className="self-center text-xl font-semibold whitespace-nowrap text-white">Museum</span>
                </Link>
                <div className="flex md:order-2">
                    <Link type="button" className="text-red-400 bg-white hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-3 md:mr-0 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800" to='/'>Keluar</Link>
                </div>
            </div>
        </nav>
        <body className="bg-black">
            <section className="bg-black">
                <div class="items-center justify-center py-8 mx-auto h-screen md:h-screen lg:py-0 bg-hero bg-no-repeat bg-cover bg-center">
                    <div className="h-screen bg-black px-6 md:px-36 lg:px-56 bg-opacity-75 py-12 md:py-14 xl:py-24">
                        <h1 class="mb-10 text-4xl font-extrabold tracking-tight leading-none text-white md:text-5xl xl:text-7xl lg:text-6xl dark:text-white">Daftar List<br/>Kunjunganmu</h1>
                        <table class="min-w-full border-collapse block md:table">
                            <thead class="block md:table-header-group">
                                <tr class="border border-grey-500 md:border-none block md:table-row absolute -top-full md:top-auto -left-full md:left-auto  md:relative ">
                                    <th class="bg-gray-600 p-2 text-white font-bold md:border md:border-grey-500 text-left block md:table-cell">No</th>
                                    <th class="bg-gray-600 p-2 text-white font-bold md:border md:border-grey-500 text-left block md:table-cell">Code</th>
                                    <th class="bg-gray-600 p-2 text-white font-bold md:border md:border-grey-500 text-left block md:table-cell">Email</th>
                                    <th class="bg-gray-600 p-2 text-white font-bold md:border md:border-grey-500 text-left block md:table-cell">Date</th>
                                    <th class="bg-gray-600 p-2 text-white font-bold md:border md:border-grey-500 text-left block md:table-cell">Actions</th>
                                </tr>
                            </thead>
                            <tbody class="block md:table-row-group">
                                <tr class="bg-gray-300 border border-grey-500 md:border-none block md:table-row">
                                    <td class="p-2 md:border md:border-grey-500 text-left block md:table-cell"><span class="inline-block w-1/3 md:hidden font-bold">No</span>1</td>
                                    <td class="p-2 md:border md:border-grey-500 text-left block md:table-cell"><span class="inline-block w-1/3 md:hidden font-bold">Code</span>32451</td>
                                    <td class="p-2 md:border md:border-grey-500 text-left block md:table-cell"><span class="inline-block w-1/3 md:hidden font-bold">Email</span>ihsanihamidah@gmail.com</td>
                                    <td class="p-2 md:border md:border-grey-500 text-left block md:table-cell"><span class="inline-block w-1/3 md:hidden font-bold">Date</span>12 Desember 2022 15:30</td>
                                    <td class="p-2 md:border md:border-grey-500 text-left block md:table-cell">
                                        <span class="inline-block w-1/3 md:hidden font-bold">Actions</span>
                                        <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-2 border border-blue-500 rounded">
                                            <Link to={{pathname: '/DetailVisiting', state: 0}}>Detail</Link>
                                        </button>
                                    </td>
                                </tr>
                                <tr class="bg-white border border-grey-500 md:border-none block md:table-row">
                                    <td class="p-2 md:border md:border-grey-500 text-left block md:table-cell"><span class="inline-block w-1/3 md:hidden font-bold">No</span>2</td>
                                    <td class="p-2 md:border md:border-grey-500 text-left block md:table-cell"><span class="inline-block w-1/3 md:hidden font-bold">Code</span>5571</td>
                                    <td class="p-2 md:border md:border-grey-500 text-left block md:table-cell"><span class="inline-block w-1/3 md:hidden font-bold">Email</span>pengajianannurahman@gmail.com</td>
                                    <td class="p-2 md:border md:border-grey-500 text-left block md:table-cell"><span class="inline-block w-1/3 md:hidden font-bold">Date</span>1 Januari 2022 16:00</td>
                                    <td class="p-2 md:border md:border-grey-500 text-left block md:table-cell">
                                        <span class="inline-block w-1/3 md:hidden font-bold">Actions</span>
                                        <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-2 border border-blue-500 rounded">
                                            <Link to={{pathname: '/DetailVisiting', state: 1}}>Detail</Link>
                                        </button>
                                    </td>
                                </tr>		
                            </tbody>
                        </table>
                        <div className="flex flex-row">
                            <button type="button" class="w-full  text-black mt-5 md:mt-9 bg-white hover:bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-primary-600 dark:hover:bg-primary-700 dark:focus:ring-primary-800">
                                <Link to="/SelectTypeVisiting">Buat Kunjungan Baru</Link>
                            </button>
                        </div>
                        <div>
                            <p className="text-sm mb-3 mt-9 h-auto tracking-tight leading-none text-white md:text-lg text-center dark:text-white xl:text-2xl">
                                Pameran Tetap Koleksi Galeri Nasional Indonesia (GNI) menampilkan karya-karya seni rupa modern dan kontemporer koleksi GNI/koleksi negara mulai era 1800-an hingga era masa kini. Dalam pameran ini ada karya para perupa kenamaan Indonesia hingga mancanegara. Di antaranya Raden Saleh, Wakidi, S. Sudjojono, Affandi, Basoeki Abdullah, Hendra Gunawan, Agus Djaja, Popo Iskandar, Srihadi Soedarsono, Widayat, Djoko Pekik, Eddie Hara, Heri Dono, Jim Supangkat, Dede Eri Supria, Krisna Murti, Anusapati, Mella Jaarsma, Victor Vasarely, Wassily Kandinsky, Hans Arp, Zao Wou-Ki, Hans Hartung, Sonia Delaunay, dan sebagainya. Pameran Tetap Koleksi GNI dikurasi oleh Bayu Genia Krishbie dan Teguh Margono (Kurator GNI) melalui tiga pendekatan kuratorial. Pertama, MONUMEN INGATAN yang menampilkan karya-karya koleksi GNI yang dikontekstualisasikan dalam perkembangan sejarah nasional. Kedua, PARIS 1959 JAKARTA 1995 menampilkan karya-karya koleksi internasional GNI yang utamanya bersumber dari dua peristiwa penting yaitu hibah seniman-seniman dunia yang berbasis di Paris pada tahun 1959 melalui Atase Kebudayaan dan Pers Bapak Ilen Surianegara, serta hibah dari seniman peserta Pameran Gerakan Non-Blok tahun 1995 di Jakarta. Ketiga, KODE /D merupakan pameran tematik yang secara berkala memamerkan sejumlah koleksi dari 20 Tahun Akusisi Karya Seni Rupa oleh GNI dalam rentang tahun 1999-2019.
                            </p>
                        </div>
                    </div>
                </div>
            </section>
            <section className="bg-black h-96"/>
            <section className="bg-black h-96"/>
            <section className="bg-black h-96"/>

        </body>
        </>
    )
}

export default ListVisiting;