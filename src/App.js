import React from 'react';
import './App.css';
import Home from './pages/Home';
import Login from './pages/Login';
import SignUp from './pages/SignUp';
import SelectTypeVisiting from './pages/SelectTypeVisiting';
import VisitingPersonal from './pages/VisitingPersonal';
import VisitingGroup from './pages/VisitingGroup';
import ListVisiting from './pages/ListVisiting';
import DetailVisiting from './pages/DetailVisiting';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

function App() {
  return (
    <>
      <Router>
        <Switch>
          <Route path='/' exact component={Home} />
          <Route path='/Login' exact component={Login} />
          <Route path='/SignUp' exact component={SignUp} />
          <Route path='/SelectTypeVisiting' exact component={SelectTypeVisiting} />
          <Route path='/VisitingPersonal' exact component={VisitingPersonal} />
          <Route path='/VisitingGroup' exact component={VisitingGroup} />
          <Route path='/ListVisiting' exact component={ListVisiting} />
          <Route path='/DetailVisiting' exact component={DetailVisiting} />
        </Switch>
      </Router>
    </>
  );
}

export default App;